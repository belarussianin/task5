package com.example.task4.di

import com.example.task4.data.repository.BankRepositoryImpl
import com.example.task4.domain.use_case.GetAtmsListByCityUseCase
import org.koin.dsl.module

val useCaseModule = module {
    factory { GetAtmsListByCityUseCase(get<BankRepositoryImpl>()) }
}