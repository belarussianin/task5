package com.example.task4.domain.repository

import com.example.task4.data.remote.dto.AtmDto

interface BankRepository {
    suspend fun getAtmsByCity(city: String): List<AtmDto>
}