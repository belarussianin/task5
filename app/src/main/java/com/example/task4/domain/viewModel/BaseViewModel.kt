package com.example.task4.domain.viewModel

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()