package com.example.task4.domain.use_case

import com.example.task4.data.remote.dto.AtmDto
import com.example.task4.domain.repository.BankRepository

class GetAtmsListByCityUseCase(
    private val repository: BankRepository
) : BaseUseCase<List<AtmDto>, String>() {
    override suspend fun run(params: String): List<AtmDto> = repository.getAtmsByCity(params)
}