package com.example.task4.presentaion.map

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.example.task4.R
import com.example.task4.data.Place
import com.example.task4.databinding.ActivityMapsBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.Circle
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.clustering.ClusterManager
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import org.koin.androidx.viewmodel.ext.android.viewModel

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var binding: ActivityMapsBinding
    private val viewModel by viewModel<MainViewModel>()
    private var circle: Circle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        lifecycleScope.launchWhenCreated {
            val gomel = LatLng(52.4313, 30.9937)
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(gomel, 10F))
            viewModel.placesList
                .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .distinctUntilChanged()
                .collect { places ->
                    if (places.isNotEmpty()) {
                        addMarkers(googleMap, places)
                        val bounds = LatLngBounds.builder()
                        places.forEach { bounds.include(it.latLng) }
                        googleMap.moveCamera(
                            CameraUpdateFactory.newLatLngBounds(
                                bounds.build(),
                                30
                            )
                        )
                    }
                }
        }
    }

    private fun addMarkers(googleMap: GoogleMap, places: List<Place>) {
        ClusterManager<Place>(this@MapsActivity, googleMap).apply {
            renderer = PlaceRenderer(
                this@MapsActivity,
                googleMap,
                this
            )

            markerCollection.setInfoWindowAdapter(MarkerInfoWindowAdapter(this@MapsActivity))

            addItems(places)
            cluster()

            setOnClusterItemClickListener { item ->
                addCircle(googleMap, item)
                return@setOnClusterItemClickListener false
            }

            googleMap.setOnCameraMoveStartedListener {
                markerCollection.markers.forEach { it.alpha = 0.3f }
                clusterMarkerCollection.markers.forEach { it.alpha = 0.3f }
            }

            googleMap.setOnCameraIdleListener {
                markerCollection.markers.forEach { it.alpha = 1.0f }
                clusterMarkerCollection.markers.forEach { it.alpha = 1.0f }

                onCameraIdle()
            }
        }
    }

    private fun addCircle(googleMap: GoogleMap, item: Place) {
        circle?.remove()
        circle = googleMap.addCircle(
            CircleOptions()
                .center(item.latLng)
                .radius(1000.0)
                .strokeColor(ContextCompat.getColor(this, R.color.purple_700))
        )
    }
}