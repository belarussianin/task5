package com.example.task4.presentaion.map

import androidx.lifecycle.viewModelScope
import com.example.task4.common.toPlace
import com.example.task4.data.Place
import com.example.task4.data.remote.dto.AtmDto
import com.example.task4.domain.use_case.GetAtmsListByCityUseCase
import com.example.task4.domain.viewModel.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.retryWhen
import kotlinx.coroutines.launch
import java.io.IOException

class MainViewModel(
    private val getAtmsListByCityUseCase: GetAtmsListByCityUseCase
) : BaseViewModel() {

    private val _atmsList = MutableStateFlow<List<AtmDto>>(emptyList())
    private val _placesList = MutableStateFlow<List<Place>>(emptyList())
    val placesList: StateFlow<List<Place>> get() = _placesList

    init {
        viewModelScope.launch(Dispatchers.IO) {
            _atmsList.onEach { atmList ->
                _placesList.value = atmList.map { it.toPlace() }
            }.launchIn(viewModelScope)
        }
        getAtmsListByCity("Гомель")
    }

    fun getAtmsListByCity(city: String) {
        viewModelScope.launch(Dispatchers.IO) {
            getAtmsListByCityUseCase.execute(city)
                .retryWhen { cause, _ ->
                    if (cause is IOException) {
                        delay(3000L)
                        true
                    } else false
                }
                .collect {
                    _atmsList.value = it
                }
        }
    }
}