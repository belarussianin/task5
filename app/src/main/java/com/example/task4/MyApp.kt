package com.example.task4

import android.app.Application
import com.example.task4.di.appModule
import com.example.task4.di.networkModule
import com.example.task4.di.useCaseModule
import com.example.task4.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MyApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@MyApp)
            modules(listOf(appModule, networkModule, useCaseModule, viewModelModule))
        }
    }
}