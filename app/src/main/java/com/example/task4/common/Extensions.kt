package com.example.task4.common

import com.example.task4.data.Place
import com.example.task4.data.remote.dto.AtmDto
import com.google.android.gms.maps.model.LatLng

fun AtmDto.toPlace() = Place(
    name = install_place,
    address = "$address_type $address $house",
    latLng = LatLng(gps_x.toDouble(), gps_y.toDouble()),
    currency = currency,
    workTime = work_time
)